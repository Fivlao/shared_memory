mod datagrams;
mod shared_buffer_client;
mod shared_buffer_server;

pub fn add(left: usize, right: usize) -> usize {
    left + right
}

pub const NONE_STR: &str = "NONE";

pub trait ToAnyhow<T, E>
    where
        E: std::error::Error + Send + Sync + 'static
{
    fn to_anyhow(self) -> anyhow::Result<T>;

    fn to_anyhow_with_context<F>(self, f: F) -> anyhow::Result<T>
        where
            F: FnOnce(&E) -> String;
}

impl<T, E> ToAnyhow<T, E> for Result<T, E>
    where
        E: std::error::Error + Send + Sync + 'static
{
    fn to_anyhow(self) -> anyhow::Result<T> {
        match self {
            Ok(val) => Ok(val),
            Err(err) => Err(anyhow::Error::new(err))
        }
    }

    fn to_anyhow_with_context<F>(self, f: F) -> anyhow::Result<T>
        where
            F: FnOnce(&E) -> String
    {
        match self {
            Ok(val) => Ok(val),
            Err(err) => Err(anyhow::anyhow!("{}", f(&err)))
        }
    }
}

pub fn get_proc_mem_path(pid: u32) -> Vec<u8> {
    let mut path = Vec::with_capacity(30);
    // let bytes: [u8; 4] = unsafe { std::mem::transmute(pid.to_be()) };
    path.append(&mut b"/proc/".to_vec());
    path.append(&mut pid.to_string().as_bytes().to_vec());
    path.append(&mut b"/mem\0".to_vec());
    path
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}
