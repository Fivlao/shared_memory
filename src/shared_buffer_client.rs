use std::io::{Read, Write};
use std::os::unix::net::UnixStream;
use anyhow::anyhow;
use crate::datagrams::{Datagram, FELT, FINISH, GET_SERVER_APP_PID, GET_SERVER_BUFF_DATA, STATUS_OK};
use crate::tools::{get_proc_mem_path, ToAnyhow};

macro_rules! size_of {
    ($($type_val:ty),+) => {
        0usize $(
            + std::mem::size_of::<$type_val>()
        )+
    };
}

struct Address {
    addr: usize,
    size_val: usize
}

impl Address {
    pub fn increment(&mut self) {
        self.addr += self.size_val;
    }
}

struct ServerBuffData {
    curr_address: Address,
    capacity: usize,
    len: usize
}

pub struct SharedBufferClient<S> {
    socket: UnixStream,
    server_app_pid: u32,
    server_buff_data: ServerBuffData,
    _val: S
}

impl<S: Datagram + Default + Sized> SharedBufferClient<S> {
    pub fn new(socket_path: &str) -> anyhow::Result<Self> {
        let _val = S::default();
        
        let mut socket = UnixStream::connect(socket_path).to_anyhow()?;
        let server_app_pid = Self::get_server_app_pid(&mut socket)?;
        let (curr_address, buff_capacity, object_size) = Self::get_server_buff_data(&mut socket)?;

        if size_of!(S) != object_size {
            return Err(anyhow!(
                "Error: size type SharedBufferClient<{0}> not equal SharedBufferServer<...>.\nSize {0}: {1}\nSize SharedBufferServer<...> type: {2}",
                std::any::type_name::<S>(), size_of!(S),  object_size
            ));
        }

        Ok(Self {
            socket: UnixStream::connect(socket_path)?,
            server_app_pid,
            server_buff_data: ServerBuffData {
                curr_address: Address {
                    addr: curr_address,
                    size_val: _val.datagram_size()
                },
                capacity: buff_capacity,
                len: 0,
            },
            _val,
        })
    }

    pub fn push(&mut self, val: S) -> anyhow::Result<()> {
        if self.server_buff_data.len == self.server_buff_data.capacity {
            self.felt()?;
        }

        let proc_mem = get_proc_mem_path(self.server_app_pid).as_ptr()
            as *const libc::c_char;

        let fd_proc_mem = unsafe { libc::open(proc_mem, libc::O_RDWR) };
        if fd_proc_mem == -1 {
            return Err(anyhow::anyhow!("Could not get access to proc mem. Pid {}", self.server_app_pid));
        }
        let write_data = val.to_datagram().as_ptr() as *const libc::c_void;
        unsafe {
            libc::lseek(
                fd_proc_mem,
                self.server_buff_data.curr_address.addr as libc::off_t,
                libc::SEEK_SET
            );

            if libc::write(fd_proc_mem, write_data, self.server_buff_data.capacity as libc::size_t) == -1 {
                return Err(anyhow::anyhow!("Could not write data to proc mem. Pid {}", self.server_app_pid));
            }
            libc::close(fd_proc_mem);
        }
        self.server_buff_data.curr_address.increment();

        Ok(())
    }

    pub fn finish(&mut self) -> anyhow::Result<()> {
        let mut len_sl: [u8; size_of!(usize)] = unsafe { std::mem::transmute(self.server_buff_data.len) };

        let mut data = Vec::with_capacity(size_of!(u8, usize));
        data.append(&mut FINISH.to_vec());
        data.append(&mut len_sl.to_vec());
        self.socket.write(&mut data).to_anyhow()?;

        let mut buff = [0u8; size_of!(u8, usize, usize)];
        self.socket.read(&mut buff).to_anyhow()?;

        if buff[0] != STATUS_OK {
            return Err(anyhow!("Error Status set size"));
        }

        let curr_address   = unsafe { *(buff[size_of!(u8)..size_of!(u8, usize)].as_ptr() as *const usize) };
        let buff_size      = unsafe { *(buff[size_of!(u8, usize)..=size_of!(u8, usize, usize)].as_ptr() as *const usize) };

        self.server_buff_data.curr_address.addr = curr_address;
        self.server_buff_data.capacity = buff_size;
        self.server_buff_data.len = 0;

        Ok(())
    }

    fn get_server_app_pid(socket: &mut UnixStream) -> anyhow::Result<u32> {
        socket.write(&GET_SERVER_APP_PID).to_anyhow()?;
        let mut buff = [0u8; size_of!(u8, u32)];
        let _ = socket.read(&mut buff).to_anyhow()?;

        if buff[0] != STATUS_OK {
            return Err(anyhow!("Get start addr and size error"));
        }

        let pid = unsafe { *(buff[size_of!(u8)..size_of!(u8, u32)].as_ptr() as *const u32) };
        Ok(pid)
    }

    fn get_server_buff_data(socket: &mut UnixStream) -> anyhow::Result<(usize, usize, usize)> {
        socket.write(&GET_SERVER_BUFF_DATA).to_anyhow()?;
        let mut buff = [0u8; size_of!(u8, usize, usize, usize)];
        let _ = socket.read(&mut buff).to_anyhow()?;

        if buff[0] != STATUS_OK {
            return Err(anyhow!("Get start addr and size error"));
        }

        let curr_address   = unsafe { *(buff[size_of!(u8)..size_of!(u8, usize)].as_ptr() as *const usize) };
        let buff_size      = unsafe { *(buff[size_of!(u8, usize)..=size_of!(u8, usize, usize)].as_ptr() as *const usize) };
        let object_size    = unsafe { *(buff[size_of!(u8, usize, usize)..=size_of!(u8, usize, usize, usize)].as_ptr() as *const usize) };

        Ok((curr_address, buff_size, object_size))
    }

    fn felt(&mut self) -> anyhow::Result<()> {
        let mut data = Vec::with_capacity(size_of!(u8));
        data.append(&mut FELT.to_vec());
        self.socket.write(&mut data).to_anyhow()?;

        let mut buff = [0u8; size_of!(u8, usize, usize)];
        self.socket.read(&mut buff).to_anyhow()?;

        if buff[0] != STATUS_OK {
            return Err(anyhow!("Error Status set size"));
        }

        let curr_address   = unsafe { *(buff[size_of!(u8)..size_of!(u8, usize)].as_ptr() as *const usize) };
        let buff_size      = unsafe { *(buff[size_of!(u8, usize)..=size_of!(u8, usize, usize)].as_ptr() as *const usize) };

        self.server_buff_data.curr_address.addr = curr_address;
        self.server_buff_data.capacity = buff_size;
        self.server_buff_data.len = 0;

        Ok(())
    }
}

#[cfg(test)]
mod test {
    use std::mem::size_of;
    use std::time::Duration;
    use crate::shared_buffer_client::SharedBufferClient;
    use crate::shared_buffer_server::SharedBufferServer;

    #[test]
    fn test() {
        println!("{}", size_of::<usize>());
        let mut server = SharedBufferServer::new("test_socket.sock", 100).unwrap();
        let mut client = SharedBufferClient::new("test_socket.sock").unwrap();
        std::thread::spawn(move || {
            server.start();
        });
        std::thread::sleep(Duration::from_secs(1));
        client.start().unwrap();

        std::thread::sleep(Duration::from_secs(1));


    }



}

// pub fn with_capacity(socket_path: &str, buff_size: usize) -> anyhow::Result<Self> {
//     let mut socket = UnixStream::connect(socket_path).to_anyhow()?;
//     let server_app_pid = Self::get_server_app_pid(&mut socket)?;
//     let _val = S::default();
//     let _ = {
//         let mut data = Vec::with_capacity(size_of!(u8, usize));
//         let mut buff_capacity_sl: [u8; size_of!(usize)] = unsafe { std::mem::transmute(_val.datagram_size()*buff_size) };
//         data.append(&mut SET_CAPACITY.to_vec());
//         data.append(&mut buff_capacity_sl.to_vec());
//         socket.write(data.as_slice()).to_anyhow()?;
//
//         let mut buff = [0u8; size_of!(u8)];
//         socket.read(&mut buff).to_anyhow()?;
//
//         if buff[0] != STATUS_OK {
//             return Err(anyhow!("Error Status set size"));
//         }
//
//         Ok::<(), anyhow::Error>(())
//     }?;
//     let (curr_address, capacity) = Self::get_server_buff_data(&mut socket)?;
//
//     Ok(Self {
//         socket: UnixStream::connect(socket_path)?,
//         server_app_pid,
//         server_buff_data: ServerBuffData {
//             curr_address: Address {
//                 addr: curr_address,
//                 size_val: _val.datagram_size()
//             },
//             capacity,
//             len: 0,
//         },
//         _val,
//     })
// }
//
// pub fn set_size(&mut self) -> anyhow::Result<()> {
//     self.socket.write(&SET_CAPACITY).to_anyhow()?;
//
//     let mut buff = [0u8; 1];
//     self.socket.read(&mut buff).to_anyhow()?;
//
//     if buff[0] != STATUS_OK {
//         return Err(anyhow!("Error Status set size"));
//     }
//
//     Ok(())
// }