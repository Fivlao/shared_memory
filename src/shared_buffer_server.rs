use std::io;
use std::io::{Read, Write};
use std::mem::size_of;
use std::os::unix::net::UnixListener;
use std::sync::mpsc;
use std::sync::mpsc::{Sender, Receiver};
use crate::datagrams::{GET_SERVER_APP_PID, GET_SERVER_BUFF_DATA, STATUS_OK, FINISH};

pub const DEFAULT_BUFF_READ: usize = size_of::<u8>() << 10;

macro_rules! size_of {
    ($($type_val:ty),+) => {
        0usize $(
            + std::mem::size_of::<$type_val>()
        )+
    };
}

struct Buffer<T> {
    buff: Vec<T>,
    len: usize
}

impl<T: Default> Buffer<T> {
    fn new(buff_size: usize) -> Self {
        Self {
            buff: vec![Default::default(); buff_size],
            len: 0
        }
    }

    pub fn buff_addr(&self) -> usize {
        self.buff.as_ptr() as usize
    }

    pub fn capacity(&self) -> usize {
        self.buff.len()
    }

    pub fn object_size(&self) -> usize {
        size_of!(T)
    }
}

pub struct SharedBufferServer<T> {
    socket: Option<UnixListener>,
    buffer_tx: Option<Sender<Buffer<T>>>,
    buffer: Buffer<T>,
}

impl<T> SharedBufferServer<T> {
    pub fn new(socket_path: &str, buff_size: usize) -> io::Result<Self> {
        let _ = std::fs::remove_file(socket_path);
        Ok(Self {
            socket: Some(UnixListener::bind(socket_path)?),
            buffer_tx: None,
            buffer: Buffer::new(buff_size)
        })
    }

    pub fn start(mut self) -> Receiver<Buffer<T>> {
        let socket = std::mem::replace(&mut self.socket, None).expect("WTF???");
        let (buffer_tx, buffer_rx) = mpsc::channel();
        self.buffer_tx = Some(buffer_tx);

        std::thread::spawn(move || for client_res in socket.incoming() {
            let mut client = if let Ok(client) = client_res { client } else {
                break;
            };
            let mut buff = [0u8; DEFAULT_BUFF_READ];

            while let Ok(size) = client.read(&mut buff) {
                if size < 1 {
                    println!("Error: Zero size read. Size: {size}");
                    continue
                }

                match [buff[0]] {
                    GET_SERVER_APP_PID => {
                        let resp_data = self.handle_get_server_app_pid();
                        let _ = client.write(resp_data.as_slice());
                    }
                    GET_SERVER_BUFF_DATA => {
                        let resp_data = self.handle_get_server_buff_data();
                        let _ = client.write(resp_data.as_slice());
                    }
                    // SET_CAPACITY => {
                    //     let resp_data = self.handle_set_capacity(&buff[1..]);
                    //     let _ = client.write(resp_data.as_slice());
                    // }
                    FINISH => {
                        let resp_data = self.handle_finish(&buff[1..]);
                        let _ = client.write(resp_data.as_slice());
                    }

                    [val] => println!("Unknown[{val}]")
                }
            }
        });

        buffer_rx
    }

    fn handle_get_server_app_pid(&self) -> Vec<u8> {
        let mut pid: [u8; size_of!(u32)] = unsafe { std::mem::transmute(std::process::id()) };
        let mut res = Vec::with_capacity(size_of!(u8, u32));
        res.push(STATUS_OK);
        res.append(&mut pid.to_vec());
        res
    }

    fn handle_get_server_buff_data(&self) -> Vec<u8> {
        let mut addr: [u8; size_of!(usize)] = unsafe { std::mem::transmute(self.buffer.buff_addr()) };
        let mut size: [u8; size_of!(usize)] = unsafe { std::mem::transmute(self.buffer.capacity()) };
        let mut ob_s: [u8; size_of!(usize)] = unsafe { std::mem::transmute(self.buffer.object_size()) };
        let mut res = Vec::with_capacity(size_of!(u8, usize, usize, usize));
        res.push(STATUS_OK);
        res.append(&mut addr.to_vec());
        res.append(&mut size.to_vec());
        res.append(&mut ob_s.to_vec());
        res
    }

    fn handle_finish(&mut self, buff: &[u8]) -> Vec<u8> {
        self.buffer.len = unsafe { *(buff[..size_of!(usize)].as_ptr() as *const usize) };
        let capacity = self.buffer.buff.capacity();
        let old_buff = std::mem::replace(&mut self.buffer, Buffer::new(capacity));
        let _ = self.buffer_tx.unwrap().send(old_buff);

        let mut addr: [u8; size_of!(usize)] = unsafe { std::mem::transmute(self.buffer.buff_addr()) };
        let mut size: [u8; size_of!(usize)] = unsafe { std::mem::transmute(self.buffer.capacity()) };
        let mut res = Vec::with_capacity(size_of!(u8, usize, usize));
        res.push(STATUS_OK);
        res.append(&mut addr.to_vec());
        res.append(&mut size.to_vec());
        res
    }
}

// fn handle_set_capacity(&mut self, buff: &[u8]) -> Vec<u8> {
//     self.buff.next_buff_capacity = unsafe { *(buff[..size_of!(usize)].as_ptr() as *const usize) };
//     if let Some(ref mut ib) = self.buff.buff1 {
//         if *ib.is_locked.borrow() == false {
//             let _ = std::mem::replace(&mut ib.buff, vec![0; self.buff.next_buff_capacity]);
//         }
//     }
//     if let Some(ref mut ib) = self.buff.buff2 {
//         if *ib.is_locked.borrow() == false {
//             let _ = std::mem::replace(&mut ib.buff, vec![0; self.buff.next_buff_capacity]);
//         }
//     }
//     vec![STATUS_OK]
// }