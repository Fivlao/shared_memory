use std::mem::size_of;

pub trait Datagram {
    fn to_datagram(self) -> Vec<u8>;

    fn datagram_size(&self) -> usize;
}

// let bytes: [u8; 4] = unsafe { std::mem::transmute(pid.to_be()) };


// pub struct Datagram<const N: usize>([u8; N]);

// type Result<T> = std::result::Result<T, ResponseError>;
pub const STATUS_ERR: u8 = 0;
pub const STATUS_OK: u8 = 1;
// const ERROR_BUFF_SIZE: usize = 30;

// struct ResponseError {
//     pub code: u8,
//     pub context: String
// }

// pub const fn resp_buff_size(ok_resp_buff_size: usize) -> usize {
//     let err_resp_buff_size = ERROR_BUFF_SIZE + size_of::<u8>();
//     std::cmp::max(ok_resp_buff_size, err_resp_buff_size)
// }

pub enum RequestCode {
    SetSizeCode = 1,
    GetAddressSize = 2
}

// pub struct Request<const S: usize, T> {
//     pub code: [u8; 1],
//     pub resp_size: usize,
//     pub resp_parce: fn([u8; S]) -> Result<T>
// }

// pub const SET_SIZE: Request<{ resp_buff_size(size_of::<u8>()) }, ()> = Request {
//     code: [3],
//     resp_size: 1,
//     resp_parce: |buff| {
//         if buff[0] == STATUS_OK {
//             Ok(())
//         } else {
//             Err(ResponseError { code: buff[0], context: String::from_utf8(buff[1..].to_vec()).unwrap_or(NONE_STR.to_string()) })
//         }
//     },
// };
//
pub const GET_SERVER_APP_PID: [u8; 1] = [3];
pub const GET_SERVER_BUFF_DATA: [u8; 1] = [4];
pub const PUSH_DATA: [u8; 1] = [5];
pub const FINISH: [u8; 1] = [6];
pub const FELT: [u8; 1] = [7];
//     { resp_buff_size(resp_buff_size(size_of::<u8>() + size_of::<(usize, usize)>())) },
//     (usize, usize)
// > = Request {
//     code: [4],
//     resp_size: size_of::<u64>() + size_of::<usize>(),
//     resp_parce: |buff| {
//         if buff[0] == STATUS_OK {
//             Ok((
//                 unsafe { *(buff[1..{ size_of::<usize>() +1 }].as_ptr() as *const usize) },
//                 unsafe { *(buff[{ size_of::<usize>() +1 }.. ].as_ptr() as *const usize) },
//             ))
//         } else {
//             Err(ResponseError { code: buff[0], context: String::from_utf8(buff[1..].to_vec()).unwrap_or(NONE_STR.to_string()) })
//         }
//     },
// };



// unsafe fn u8_slice_as_any<T: Sized>(slice: &[u8]) -> &T {
//     &*{ slice.as_ptr() as *const T }
// }


#[cfg(test)]
mod test {
    use std::mem::size_of;

    #[test]
    fn test_mem() {
        let mut vec = vec![123u8, 78];

        let a = unsafe { vec.as_ptr() } as u64;
        let b = unsafe { *((a + size_of::<u8>() as u64) as *const u8) };


        println!("a: {a}, b: {b}")

        // #[derive(Clone)]
        // struct Things {
        //     x: u8,
        //     y: u16,
        // }
        // let data = unsafe { any_as_u8_slice(Things { x: 3, y: 45 }) };
        // println!("data: {data:?}");
        // let thing = unsafe { u8_slice_as_any::<Things>(&data) };
        // println!("x: {}, y: {}", thing.x, thing.y);
        //
        // let data: [u8; size_of::<AtomicU64>()] = unsafe { any_as_u8_slice(AtomicU64::new(3312345678)) };
        // println!("data: {:?}", data.clone());
        // let atomic = unsafe { u8_slice_as_any::<AtomicU64>(&data) };
        // println!("AtomicU64: {:?}", atomic);

        // let val = std::mem::size_of::<AtomicU64>();
        // let a = ;
        // println!("size_of::<AtomicU64> = {}", val)
    }
}